export class CalcData {
  public initialInvestment: number;
  public futureValue: number;
  public totalDeposit: number;
  public totalInterest: number;
  public currency: string;
  public effectiveRate: number;
  public data: any;
  public beginning?: boolean;

  constructor(presentValue, futureValue, totalDeposit, totalInterest, currency, effectiveRate, data, beginning?) {
    this.initialInvestment = presentValue;
    this.futureValue = futureValue;
    this.totalDeposit = totalDeposit;
    this.totalInterest = totalInterest;
    this.currency = currency;
    this.effectiveRate = effectiveRate;
    this.data = data;
    this.beginning = beginning;
  }
}
