export interface UserSettingsData {
  name: string;
  baseCurrency: string;
  initialBalanceValue: number;
  totalInterestValue: number;
  depositsValue: number;
  futureValue: number;
  initialBalance: number[];
  deposits: number[];
  totalInterest: number[];
  timeLabels: number[];
  effectiveRate: number;
  id?: string;
}

export class UserSettings {
  public name: string;
  public baseCurrency: string;
  public initialBalanceValue: number;
  public totalInterestValue: number;
  public depositsValue: number;
  public futureValue: number;
  public initialBalance: number[];
  public deposits: number[];
  public totalInterest: number[];
  public timeLabels: number[];
  public effectiveRate: number;

  constructor(
    name, baseCurrency, initialBalanceValue, totalInterestValue, depositsValue,
    futureValue, initialBalance, deposits, totalInterest, timeLabels, effectiveRate) {
      this.name = name;
      this.baseCurrency = baseCurrency;
      this.initialBalanceValue = initialBalanceValue;
      this.totalInterestValue = totalInterestValue;
      this.depositsValue = depositsValue;
      this.futureValue = futureValue;
      this.initialBalance = initialBalance;
      this.deposits = deposits;
      this.totalInterest = totalInterest;
      this.timeLabels = timeLabels;
      this.effectiveRate = effectiveRate;
  }

  setUpdateSettings(): UserSettingsData {
    const data = {
      name: this.name,
      baseCurrency: this.baseCurrency,
      initialBalanceValue: this.initialBalanceValue,
      totalInterestValue: this.totalInterestValue,
      depositsValue: this.depositsValue,
      futureValue: this.futureValue,
      initialBalance: this.initialBalance,
      deposits: this.deposits,
      totalInterest: this.totalInterest,
      timeLabels: this.timeLabels,
      effectiveRate: this.effectiveRate
    };
    return data;
  }
}
