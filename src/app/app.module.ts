import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { CalculatorComponent } from './calculator/calculator.component';
import { HttpClientModule } from '@angular/common/http';
import { CompoundInterestChartComponent } from './compound-interest-chart/compound-interest-chart.component';
import { AppService } from './app.service';
import { SaveSettingsDialogComponent } from './compound-interest-chart/save-settings-dialog/save-settings-dialog.component';
import { UserSettingsService } from './compound-interest-chart/user-settings.service';

// Echarts
import { NgxEchartsModule } from 'ngx-echarts';
import * as echarts from 'echarts';

// Angular Material
import { MatInputModule } from '@angular/material/input';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [
    AppComponent,
    CalculatorComponent,
    CompoundInterestChartComponent,
    SaveSettingsDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    MatInputModule,
    MatButtonToggleModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatRadioModule,
    MatDialogModule,
    NgxEchartsModule.forRoot({ echarts })
  ],
  providers: [UserSettingsService, AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
