import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserSettingsData } from 'src/app/models/userSettingsData.model';

@Component({
  selector: 'app-save-settings-dialog',
  templateUrl: './save-settings-dialog.component.html',
  styleUrls: ['./save-settings-dialog.component.scss']
})
export class SaveSettingsDialogComponent implements OnInit {
  public name: string;

  constructor(
    public dialogRef: MatDialogRef<SaveSettingsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserSettingsData) { }

  public ngOnInit(): void {
  }

  public onCancel(): void {
    this.dialogRef.close();
  }
}
