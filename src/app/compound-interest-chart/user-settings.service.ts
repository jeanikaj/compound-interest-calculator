import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { UserSettingsData } from '../models/userSettingsData.model';

@Injectable()
export class UserSettingsService {
  private userId = '123';
  private userDoc: AngularFirestoreDocument<any>;
  private userGraphCollection: AngularFirestoreCollection<any>;
  public userSettings: Observable<UserSettingsData>;

  constructor(private firestore: AngularFirestore) {}

  public getUserSettings() {
    this.userGraphCollection = this.firestore.collection(`userSettings/${this.userId}/graphSettings`);
    return this.userGraphCollection.valueChanges({idField: 'id'});
  }

  public postUserData(data: UserSettingsData): void {
    this.userGraphCollection.add(data)
      .then(() => {
        alert('Successfully added graph to user settings');
      })
      .catch((error) =>
        alert('Failed to add graph to user settings'));
  }

  public deleteGraph(id: string) {
    this.userDoc = this.firestore.doc(`userSettings/${this.userId}/graphSettings/${id}`);
    this.userDoc.delete()
    .then(() => {
      alert('Successfully deleted graph from user settings');
    })
    .catch((error) =>
      alert('Failed to remove graph from user settings'));
  }

}
