import { Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { EChartOption } from 'echarts';
import { CalcData } from '../models/calculationData.model';
import { AppService } from '../app.service';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { SaveSettingsDialogComponent } from './save-settings-dialog/save-settings-dialog.component';
import { UserSettings, UserSettingsData } from '../models/userSettingsData.model';
import { UserSettingsService } from './user-settings.service';

@Component({
  selector: 'app-compound-interest-chart',
  templateUrl: './compound-interest-chart.component.html',
  styleUrls: ['./compound-interest-chart.component.scss'],
})
export class CompoundInterestChartComponent implements OnInit, OnChanges, OnDestroy {
  @Input() public calcData: CalcData;
  @Input() public userData: UserSettingsData;

  // For graph
  public initialBalance = [];
  public deposits = [];
  public totalInterest = [];
  public timeLabels = [];
  public chartOption: EChartOption;
  public exchangeRate = 1;
  public exchangeRates;
  public selectedBaseCurrency: string;
  public name: string;

  // Subscriptions
  public exchangeRateSubscription: Subscription;

  // For info
  public currency = 'GBP';
  public futureVal: number;
  public presentVal: number;
  public depositsVal: number;
  public interestVal: number;
  public effectiveRate: number;

  public get title(): string {
    if (this.userData && this.userData.name) {
      return this.userData.name;
    }

    return 'Projection calculation';
  }

  constructor(
    private appService: AppService,
    private userSettingsService: UserSettingsService,
    public dialog: MatDialog
  ) {
  }

  public ngOnInit(): void {
    if (this.calcData && this.calcData.currency) {
      this.selectedBaseCurrency = this.calcData.currency;
    }
    this.setSummaryData();
    this.getExchangeRates();
    this.setChartData();
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (this.calcData) {
      this.getExchangeRates();
    }
    this.setSummaryData();
    this.setChartData();
  }

  public ngOnDestroy(): void {
    if (this.exchangeRateSubscription) {
      this.exchangeRateSubscription.unsubscribe();
    }
  }

  protected setSummaryData() {
    if (this.calcData) {
      this.currency = this.selectedBaseCurrency;
      this.futureVal = this.calcData.futureValue * this.exchangeRate;
      this.presentVal = this.calcData.initialInvestment * this.exchangeRate;
      this.depositsVal = this.calcData.totalDeposit * this.exchangeRate;
      this.interestVal = this.calcData.totalInterest * this.exchangeRate;
      this.effectiveRate = this.calcData.effectiveRate;
    } else if (this.userData) {
      this.currency = this.userData.baseCurrency;
      this.futureVal = this.userData.futureValue * this.exchangeRate;
      this.presentVal = this.userData.initialBalanceValue * this.exchangeRate ;
      this.depositsVal = this.userData.depositsValue * this.exchangeRate;
      this.interestVal = this.userData.totalInterestValue * this.exchangeRate;
      this.effectiveRate = this.userData.effectiveRate;
    }
  }

  public setChartData() {
    this.initialBalance = [];
    this.deposits = [];
    this.totalInterest = [];
    this.timeLabels = [];

    this.setSummaryData();

    if (this.userData) {
      for (const balance of this.userData.initialBalance) {
        this.initialBalance.push((balance * this.exchangeRate).toFixed(2));
      }
      for (const deposit of this.userData.deposits) {
        this.deposits.push((deposit * this.exchangeRate).toFixed(2));
      }
      for (const interest of this.userData.totalInterest) {
        this.totalInterest.push((interest * this.exchangeRate).toFixed(2));
      }
      this.timeLabels = this.userData.timeLabels;
      this.name = this.userData.name;
    }
    if (this.calcData) {
      for (let i = 0; i < this.calcData.data.length; i++) {
        const element = this.calcData.data[i];
        this.initialBalance.push((element.initialInvestment * this.exchangeRate).toFixed(2));
        this.deposits.push((element.deposits * this.exchangeRate).toFixed(2));
        this.totalInterest.push((element.interestEarned * this.exchangeRate).toFixed(2));
        this.timeLabels.push(i + 1);
      }
    }

    this.chartOption = {
      color: [
        '#3873d8', '#53d3d3', '#325da5'
      ],
      tooltip: {
        trigger: 'axis',
        confine: true,
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {
        data: ['Initial balance', 'Deposits', 'Total interest']
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      yAxis: {
        type: 'value'
      },
      xAxis: {
        type: 'category',
        data: this.timeLabels
      },
      series: [
        {
          name: 'Initial balance',
          type: 'bar',
          stack: '总量',
          label: {
            show: false,
            position: 'insideRight'
          },
          data: this.initialBalance,
        },
        {
          name: 'Deposits',
          type: 'bar',
          stack: '总量',
          label: {
            show: false,
            position: 'insideRight'
          },
          data: this.deposits
        },
        {
          name: 'Total interest',
          type: 'bar',
          stack: '总量',
          label: {
            show: false,
            position: 'insideRight'
          },
          data: this.totalInterest
        }
      ],
    };
  }

  public getExchangeRates() {
    this.exchangeRateSubscription = this.appService.getExchangeRate(this.currency)
      .subscribe((res) => {
        this.exchangeRates = res.rates;
        this.calculateExchangeRate(this.currency);
      },
        (err) => {
          console.log('Failed to retrieve currencies');
        }
      );
  }


  public calculateExchangeRate(baseCurrency): any {
    // Calculate exchange rates for base currency
    this.selectedBaseCurrency = baseCurrency;
    Object.keys(this.exchangeRates).forEach((key) => {
      if (key === baseCurrency) {
        this.exchangeRate = this.exchangeRates[key];
        this.setChartData();
      }
    });
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(SaveSettingsDialogComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.name = result;

      if (result) {
        const settings = new UserSettings(
          this.name,
          this.selectedBaseCurrency,
          this.presentVal,
          this.interestVal,
          this.depositsVal,
          this.futureVal,
          this.initialBalance,
          this.deposits,
          this.totalInterest,
          this.timeLabels,
          this.effectiveRate
        ).setUpdateSettings();
        this.userSettingsService.postUserData(settings);
      }
    });
  }

  public deleteGraph(id: string) {
    this.userSettingsService.deleteGraph(id);
  }
}
