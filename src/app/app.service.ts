import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CalcData } from './models/calculationData.model';
import { Observable } from 'rxjs';

@Injectable()
export class AppService {
  selectedBaseCurrency;
  exchangeRates;
  exchangeRate;
  currency = 'GBP';
  chartData: CalcData;

  constructor(private http: HttpClient) { }

  public getExchangeRate(currency): Observable<any> {
    return this.http.get(`https://api.exchangeratesapi.io/latest?base=${currency}`);
  }

  public depositFutureValue(PMT, i, n, t, beginning?: boolean): number {
    let fV;
    if (beginning) {
      fV = PMT * (Math.pow((1 + i / n), (n * t)) - 1) / (i / n) * (1 + i / n);
    } else {
      fV = PMT * (Math.pow((1 + i / n), (n * t)) - 1) / (i / n);
    }

    return fV;
  }

  public principalFutureValue(P, i, n, t): number {
    const fV = P * Math.pow(1 + (i / n), (n * t));
    return fV;
  }

  public effectiveAnnualInterestRate(i, n): number {
    const EAR = (Math.pow(1 + (i / n), n) - 1);
    return EAR * 100;
  }

  public calculateInterestValues(presentValue, interval, rate, years, deposit, currency, beginning?) {
    this.currency = currency;
    // Compound interest for principal
    const principalFV = this.principalFutureValue(presentValue, rate, interval, years);

    // Compound interest for monthly deposits
    const depositFV = this.depositFutureValue(deposit, rate, interval, years, beginning);
    const EAR = this.effectiveAnnualInterestRate(rate, interval);

    // Calculate total interest
    const totalDeposits = deposit * 12 * years;
    const totalInterest = principalFV + depositFV - totalDeposits - presentValue;

    // Future value
    const futureValue = (principalFV + depositFV);

    // Calculate interest earned
    const effectiveRate = EAR.toFixed(2);

    // Get data for chart
    const yearsData = this.getYearsData(presentValue, years, rate, deposit, interval, currency, beginning);

    return this.getChartData(presentValue, futureValue, totalInterest, effectiveRate, totalDeposits, currency, yearsData);
  }

  public getYearsData(presentValue, years, rate, deposit, interval, currency, beginning?) {
    // Calculate and set data for each year up until n years
    const yearArr = [];

    for (let i = 1; i <= years; i++) {
      const principalFV = this.principalFutureValue(presentValue, rate, interval, i);
      const depositFV = this.depositFutureValue(deposit, rate, interval, i, beginning);
      const fv = principalFV + depositFV;
      const totalMonthlyDeposits: number = (deposit * 12) * i;
      const totalInterest = fv - presentValue - totalMonthlyDeposits;
      yearArr.push(
        {
          year: i,
          initialInvestment: presentValue,
          futureValue: fv,
          deposits: totalMonthlyDeposits.toFixed(2),
          interestEarned: totalInterest.toFixed(2)
        }
      );
    }

    this.currency = currency;

    return yearArr;
  }

  getChartData(presentValue, futureValue, totalInterest, effectiveRate, totalDeposits, currency, yearsData) {
    this.chartData = new CalcData(
      presentValue,
      futureValue,
      totalDeposits,
      totalInterest,
      currency,
      effectiveRate,
      yearsData
    );
  }

  getExchangeRates() {
    this.getExchangeRate(this.currency)
    .subscribe((res: any) => {
      this.exchangeRates = res.rates;
    });
  }

  calculateExchangeRate(baseCurrency): any {
    // Get exchange rates for base currency
    this.selectedBaseCurrency = baseCurrency;
    Object.keys(this.exchangeRates).forEach((key) => {
      if (key === baseCurrency) {
        this.exchangeRate = this.exchangeRates[key];
      }
    });
  }
}
