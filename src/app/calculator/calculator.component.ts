import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AppService } from '../app.service';
import { CalcData } from '../models/calculationData.model';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})

export class CalculatorComponent implements OnInit {
  public presentValue: number;
  public currency = 'GBP';

  public get data(): CalcData {
    return this.appService.chartData;
  }

  public compoundOptions = [
    {option: 'Monthly', value: 12}
  ];

  public calculatorForm = this.fb.group({
    initialVal: [null, Validators.required],
    interestRate: [null, [Validators.required, Validators.maxLength(4), Validators.pattern(/^\d{1,2}(\.\d{1,2})?$/)]],
    depositVal: [null, Validators.required],
    period: [null, Validators.required],
    interval: [12, Validators.required],
    currency: [this.currency,  Validators.required],
    beginning: ['beginning']
  });

  public get symbol(): string {
    switch (this.calculatorForm.get('currency').value) {
      case 'USD':
        return '$';
      case 'EUR':
        return '€';
      case 'JPY':
        return '¥';
      default:
      return '£';
    }
  }

  constructor(
    private fb: FormBuilder,
    private appService: AppService) {
  }

  public ngOnInit(): void {
  }

  public onSubmit() {
    this.presentValue = this.calculatorForm.get('initialVal').value;
    const interval = this.calculatorForm.get('interval').value;
    const rate = this.calculatorForm.get('interestRate').value / 100;
    const years = this.calculatorForm.get('period').value;
    const deposit = this.calculatorForm.get('depositVal').value;
    const currency = this.calculatorForm.get('currency').value;
    const beginning = this.calculatorForm.get('beginning').value === 'beginning';

    this.appService.calculateInterestValues(this.presentValue, interval, rate, years, deposit, currency, beginning);
  }
}
