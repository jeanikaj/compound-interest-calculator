import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserSettingsService } from './compound-interest-chart/user-settings.service';
import { UserSettingsData } from './models/userSettingsData.model';
import { Subscription } from 'rxjs';
import { FocusMonitor, ConfigurableFocusTrapFactory } from '@angular/cdk/a11y';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  public userData: UserSettingsData[];
  public userSettingsSubscription: Subscription;

  constructor(
    private userSettingsService: UserSettingsService,
    private focusTrap: ConfigurableFocusTrapFactory,
    private focusMonitor: FocusMonitor
  ) {}

  public ngOnInit(): void {
    this.getUserSettings();
  }

  public ngOnDestroy(): void {
    if (this.userSettingsSubscription) {
      this.userSettingsSubscription.unsubscribe();
    }
  }

  public getUserSettings(): void {
    this.userSettingsSubscription = this.userSettingsService.getUserSettings()
    .subscribe(res => {
      this.userData = res;
    });
  }
}
