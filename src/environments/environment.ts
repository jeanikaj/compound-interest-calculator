// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBinUVWxB3oIcJvqBOin8ZKqBF4_xqC-F4',
    authDomain: 'interest-calculator-da1bf.firebaseapp.com',
    databaseURL: 'https://interest-calculator-da1bf.firebaseio.com',
    projectId: 'interest-calculator-da1bf',
    storageBucket: 'interest-calculator-da1bf.appspot.com',
    messagingSenderId: '502962697074',
    appId: '1:502962697074:web:79e6ecc822ffc281d0a3eb',
    measurementId: 'G-7FCN3SSX0H'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
