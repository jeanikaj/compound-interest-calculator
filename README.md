# InterestCalculatorApp

## Description

The Interest Calculator App is a compound interest calculator app built with Angular 9, Angularfire2, Angular Material and ECharts.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.7.

### Dependencies

Node.js v12.16.1

## Get started

### Clone the repo

```shell
git clone https://gitlab.com/jeanikaj/compound-interest-calculator.git
cd compound-interest-calculator
```

### Install node modules

Install the `npm` packages described in the `package.json`:

```shell
npm install
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running ng-lint

Run `ng lint` to run linter.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
